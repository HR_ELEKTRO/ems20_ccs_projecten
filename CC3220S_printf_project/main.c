/*
 * Copyright (C) 2024, Hogeschool Rotterdam
 * All rights reserved.
 */

#include <stdio.h>

int main(void)
{
    printf("De CC3220S LaunchPad zegt:\n");
    printf("Hallo!\n");

    // Wacht tot het programma gestopt wordt in de debugger.
    while (1);
    return 0;
}
